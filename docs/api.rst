API Reference
=============

condition module
------------------------------------------------

.. automodule:: condition
   :members:

.. autoclass:: condition.FieldList
   :members:

.. autoclass:: condition.Condition
   :members:

.. autoclass:: condition.FieldCondition
   :members:

.. autoclass:: condition.CompositeCondition
   :members:


.. autoclass:: condition.And
   :members:

.. autoclass:: condition.Or
   :members:

condition.sql module
------------------------------------------------

.. autofunction:: condition.sql.render_sql


