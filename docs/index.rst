.. condition documentation master file, created by
   sphinx-quickstart on Mon May 14 19:39:31 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the Condition Package
=====================================
This package can be used to construct a condition object in a user friendly way. The condition
object can be passed as a parameter and later used to query pandas dataframes, filter pyarrow
partitions or to generate where conditions in SQL. 
It takes care of formating and syntax for you.


.. toctree::
   :maxdepth: 3
   :caption: Contents:

   intro.rst
   usage.rst
   api.rst
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
