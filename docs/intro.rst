Introduction
************
Welcome to the Condition package. 

Goals
------------
This package aims to achieve the following goals:

#. Provides a user friendly way to construct condition objects. Support common operators: ``<, <=, >, >=, ==, in, not in``.
#. Supports composite conditions with ``And``, ``Or`` with arbitrary structure;
#. Supports various usage contexts, for example, pandas dataframe filtering, SQL generation and pyarrow partition filtering.
#. Supports extensibility to new usage contexts.

Benefits
------------
#. A condition object can be passed to the back end.
#. A condition object can be interpreted consistently in different contexts.
#. A support usage context takes care of formatting and syntax details.

Installing
----------
This project is distributed via pip. To get started:

.. code::

    pip install condition

To install jinja2 package used for sql generation, do the following

.. code::

    pip install "condition[sql]"

To install all packages for development, do the following

.. code::

    pip install "condition[dev]"

Author
------
Weiyang Zhao <wyzhao@gmail.com>

License
-------
This package uses the MIT license.
Check LICENSE file.

Contributing
------------
If you'd like to contribute, fork the project, make a patch and send a merge
request. Please see ``CONTRIBUTING.md`` in the root of this project.