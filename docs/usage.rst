Usage Guide
==============================
This document illustrates how to use the condition package.

A Sample Dataframe
-------------------------
For illustration purpose, let's first get a sample dataframe.

.. ipython:: python

    import os
    import pandas as pd
    from condition import *
    df = get_test_df()
    df.tail(10)

The data frame has four index levels: date, A, B, C and a single column: value. 
The below conditions are constructed based on this data frame.


Basics
-------------------------

Field
~~~~~~~~~~~~~~~~~~~~~~~~~
All conditions are defined on fields. Field is a simple object created from a str and denotes a column
in a pandas DataFrame or a sql column.

.. ipython:: python

    Field('Col1')


Field List
~~~~~~~~~~~~~~~~~~~~~~~~~
To make it easy to refer to all fields, you can create a field list. 
You can do it with a list of strings.

.. ipython:: python

    fl = FieldList(['date', 'A', 'B', 'C', 'value'])
    fl

You can also create a field list from a dataframe. The fields are the index names plus the columns,
namely, the index levels and columns are treated in the same way.

.. ipython:: python

    fl2 = FieldList.from_df(df)
    fl2


After creating a field list, you can refer to its field as an attribute.

.. ipython:: python

    fl.A
    fl.B
    fl.date
    fl.value

You can also create a ``Field`` directly. But using a ``FieldList`` gives you the benefit of validation
and autocompletion.

.. ipython:: python

    Field('A') # same as fl.A
    Field('B') # same as fl.B


Field Condition
~~~~~~~~~~~~~~~~~~~~~~~~~
A field condition is formed by:

| ``Field`` ``Operator`` ``value``

All comparision operators (``<,<=,>,>=,==,!=``)
are supported. Besides, ``in`` and ``not in`` semantics are supported by using ``==`` and ``!=`` with a collection, 
such as a ``list``, a ``set`` or a ``tuple``. Please note that the type of each item in the collection must be the same.

When printed, a condition's ``__str__`` method is called which returns a SQL where condition clause. When directly referenced, 
its ``__repr__`` method is called which returns a str that can be parsed back to a condition. In the repr format,
``T()`` is for converting a str to a datetime.

.. ipython:: python

    cond = (fl.A == 'a1')  # cond is a FieldCondtion variable
    print(cond)  # __str__ format
    cond         # __repr__ format
    # typically you need not to assign it to a variable. We will see later.
    fl.value >= 0
    fl.date <= pd.to_datetime('20020101')
    # in and not_in
    fl.A == (['a1', 'a3'])
    fl.B != ( ['b3', 'b5'])

Value Types
^^^^^^^^^^^
The type of value in the ``FieldCondition`` is important. For ``in`` and ``not in``, 
all the elements of the collection need to be the same type. The type decides two things:

# how to format a result string
# how to convert a string to the correct type before comparision

Currently supported types are: all numeric types, a string (quoted), a datetime or pd.Timestamp.


And Condition
~~~~~~~~~~~~~~~~~~~~~~~~~
An ``And`` condition can be created by a constructor with a list of conditions.

.. ipython:: python

    and1 = And(
            [
                fl.date >= pd.to_datetime('20000101'),
                fl.date <= pd.to_datetime('20000131'),
                fl.A == 'a1 a3'.split(),
                fl.C != 'c3 c5'.split(),
            ]
        )
    and1         # repr format
    print(and1)  # str format

or by using ``&`` operator:

.. ipython:: python

    and2 = ((fl.date >= pd.to_datetime('20000101')) 
            & (fl.date <= pd.to_datetime('20000131'))
            & (fl.A == 'a1 a3'.split()) 
            & (fl.C != 'c3 c5'.split())
            )
    print(and2)


.. note::
    Although it seems ``&`` is more convenient, it is a bitwise operator and its precedence is 
    higher than ``==``, ``>=`` and etc.. This can cause surprising errors.
    For example, ``fl.A == 'a1' & fl.B == 'b1'`` will report error because it is interpreted as
    ``fl.A == ('a1' & fl.B) == 'b1'``. For this reason, it is safer to use the first approach,
    or the constructor with a list of conditions.  If you still want to use ``&``, make sure you
    use ``()`` to surround the field conditions such as: ``(fl.A == 'a1') & (fl.B == 'b1')``.

The above two yield the same result. But the first approach can be more efficient because the second
approach creates one intermediate immutable condition object for each two conditions.


Or Condition
~~~~~~~~~~~~~~~~~~~~~~~~~
An ``Or`` condition can be created by a constructor with a list of conditions.

.. ipython:: python

    or1 = Or(
            [
                fl.date >= pd.to_datetime('20000101'),
                fl.C != ('c3 c5'.split()),
            ]
        )


or by using ``|`` operator:

.. ipython:: python

    or2 = (fl.date >= pd.to_datetime('20000101')) | (fl.C != ('c3 c5'.split()))
    or2


The above two yield the same result. But the first approach can be more efficient because the second
approach creates one intermediate immutable condition object for each two conditions.

.. note::
    Although it seems ``|`` is more convenient, it is a bitwise operator and its precedence is 
    higher than ``==``, ``>=`` and etc.. This can cause surprising errors.
    For example, ``fl.A == 'a1' & fl.B == 'b1'`` will report error because it is interpreted as
    ``fl.A == ('a1' | fl.B) == 'b1'``. For this reason, it is safer to use the first approach,
    or the constructor with a list of conditions.  If you still want to use ``|``, make sure you
    use ``()`` to surround the field conditions such as: ``(fl.A == 'a1') | (fl.B == 'b1')``.

Features
-------------------------
Immutability
~~~~~~~~~~~~~~~
A condition object is immutable after construction. You can use it in multi threads safely.

Serializablility
~~~~~~~~~~~~~~~~
A condition object can be  serialized(or pickled) to storage or for network transport.

Equality Test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You can check if two condition objects are the same with ``==``.
Note that for sub conditions and collection values, order does not matter as shown below.
 
.. ipython:: python

    (fl.A == ['a1', 'a5']) == (fl.A == ('a5', 'a1'))
    cond1 = And([fl.A == ['a1', 'a5'], fl.C == {'c2', 'c4'}]) 
    cond2 = And([fl.C == ('c4', 'c2'), fl.A == ('a5', 'a1')]) 
    cond1 == cond2


Hashcodes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You can use ``hash()`` to get a hashcode for a condition object. Therefore a condition object can be used 
as a key in a dict and set. The hashcode is also order independent as for the equality test.

Non Standard Field Names
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
If a field name is not a valid identifier, for example, "with space", "state.ca", in ``FieldList``,
it will be converted to an identifier by replacing special characters with "_". When there is a conflict,
a number is added to make it unique. The above names become ``fl.with_space``, ``fl.state_ca``. Alternatively, 
you can get the field with the name directly, ``fl["with space"]`` or ``Field("with space")``. 


The original field name will be enclosed with ``"`` (double quote)
when sql is rendered or ````` (backtick) when ``to_df_query()`` is rendered. On the other hand,
``to_pyarrow_filter()`` needs no special treatment.

For sql, different DB may need different way to enclose such names, if your DB needs a different way to
enclose special columns, you have two choices:

#. Use ``dbmap``;
#. Set ``SQL_ID_DELIM_LEFT`` and ``SQL_ID_DELIM_RIGHT`` env variables.

See below examples:

.. ipython:: python

    fl = FieldList(["13abc", "with space", "with.space", "params.p1"])
    fl._13abc.name
    fl.with_space.name
    fl["with space"].name
    fl.with_space1.name
    fl.params_p1.name
    c = (fl.with_space > 2)
    print(c.to_df_query())
    print(c.to_sql_where_condition())
    # option 1
    print(c.to_sql_where_condition(db_map={"with space" : "`with space`"}))
    # option 2
    os.environ["SQL_ID_DELIM_LEFT"] = "["
    os.environ["SQL_ID_DELIM_RIGHT"] = "]"
    print(c.to_sql_where_condition())


``And``, ``Or`` Flatten
~~~~~~~~~~~~~~~~~~~~~~~~~
To simplify the condition object, when you create a nested ``And`` or ``Or`` condition, 
The structure is automatically flattened. It means that ``A and (B and (C and D)))`` is 
flattened to ``A and B and C and D``. Similarly, ``A or (B or (C or D)))`` is flattened 
to ``A or B or C or D``.


Example:

.. ipython:: python

    fl = FieldList.from_df(df)
    Or(
            [
                Or([fl.date >= pd.to_datetime('20000101'),
                    fl.C != ('c3 c5'.split())]),
                Or([fl.A=='a1']),
                fl.B == 'b2'
            ]
        )
    And(
            [
                And([fl.date >= pd.to_datetime('20000101'),
                    fl.C != ('c3 c5'.split())]),
                And([fl.A=='a1']),
                fl.B == 'b2'
            ]
        )

As shown above, a nested ``And`` or ``Or`` condition is automatically flattened
to be a single level ``And`` or ``Or`` condition.

Normalization
~~~~~~~~~~~~~~~~~~~~~~~~~
The method ``normalize()`` converts the condition to be one of the following: 

- a ``FieldCondition``
- an ``And`` with a list of sub ``FieldCondition``
- an ``Or`` with a list of sub conditions as defined above.

In some cases, e.g., pyarrow filtering, the above restrictions must be followed.
Any condition can be normalized to the above form in an equalivent way.

See below example and also its visualiation in the next section:

.. ipython:: python

    cond1 = And([
        fl.A == 'a1',
        Or([
            fl.B == 'b1',
            fl.C == 'c1',
            And([
                fl.value >= 3,
                fl.value <= 5
            ])
        ]),
        Or([
            fl.B == 'b2',
            fl.C == 'c2'
        ])
    ])
    print(cond1)
    print(cond1.normalize())

Visualization
~~~~~~~~~~~~~~~~~~~~
You can visaulize the condition structure with ``cond.visualize()`` method.
It requires an extra package ``graphviz`` in your system environment.
See `graphviz <https://www.graphviz.org/download/>`_ for installation instructions.



.. figure:: _static/cond1.png
   :scale: 100 %
   :alt: cond1 visualization
   :align: center
   :target: _static/cond1.png

   The ``cond1`` in the previous section

.. figure:: _static/cond1-normalized.png
   :scale: 100 %
   :alt: cond1 normalized visualization
   :align: center
   :target: _static/cond1-normalized.png

   The visualization of ``cond1.normalize()``.

String <=> Condition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
As mentioned before, when printed, a condition's ``__str__`` method is called which returns a SQL where condition clause. 
When directly referenced or ``repr()`` is called, it returns a string which can be parsed back to a condition. In the repr format,
``T()`` is for converting a str to a datetime.
The ``parse()`` method is safe in that no irrelvant function/class can be called in the string. When called, a ``fl: FieldList``
variable must be presented, although the variable name can be customized.

Examples:

.. ipython:: python

    fl = FieldList(['date', 'A', 'B', 'C', 'value'])
    cond1 = Condition.parse("(fl.A>T('20000101')) & (fl.B==['b1', 'b2'])  & (fl.C>=100)")
    cond1
    Condition.parse("And([fl.A>T('20000101'), fl.B==['b1', 'b2'], fl.C>=100])")
    Condition.parse(repr(cond1))

    try:
        Condition.parse("dir()")   # unsafe call should result in an error.
    except:
        print("An error")


Split of a Condition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The method ``split()`` splits the condition to a new condition which only contains the passed in fields.
This method is used in the following scenario:

#. A combined data item is joined from two or more sub data sources.
#. The condition is defined on the combined data.
#. Use this method to get a split condition to be applied to the sub data sources with the fields list in the sub data sources.
#. After the data is joined, apply the original condition on the combined data.

See the below example:

.. ipython:: python

    cond = And(
        [
            fl.A == "a1",
            Or([fl.B == "b1", fl.C == "c1", And([fl.value >= 3, fl.value <= 5])]),
            Or([fl.B == "b2", fl.C == "c2"]),
        ]
    )
    cond1 = cond.split('notExisted')
    assert cond1 == EMPTY_CONDITION
    cond2 = cond.split(["A"])
    print(cond2)
    cond3 = cond.split(["B", "C"])
    print(cond3)

In the above example:

#. ``cond1`` does not contain any field in ``cond``, so it is split to an empty condition which means no row will be filtered out.
#. ``cond2`` only contains field ``A``, so it is split to the first sub condition ``fl.A == "a1"``. 
#. ``cond3`` does not contain field ``value``, therefore, ``And([fl.value >= 3, fl.value <= 5])]`` is ignored and assumed to be ``True``, then the first ``Or`` condition is evaluated to ``True``. Thus only the second ``Or`` condition is kept.

Usage Contexts
-------------------------

Evaluation
~~~~~~~~~~~~~~~~~~~~
The ``eval()`` method evaluates the condition to ``True`` or ``False``
against the data record you provide. The data record maps from a field
to a value to be compared with the ``FieldCondition``'s.
Optionally, you can ask it to convert value in record_dict to the ``FieldCondition``
value type before comparision. Sometimes such conversion 
is needed, for example, in pyarrow partition filtering.

Note that if you have a large number of records, the recommended way to evaluate all
of them in batch mode is to create a pandas DataFrame from the records and then call
``condition.query(df)``. You can install ``numexpr`` package for much faster performance.

For example, the below code implements hive partition filtering:

.. ipython:: python

    paths = [
                'A=a1/B=b1/C=c1',
                'A=a2/B=b1/C=c1',
                'A=a3/B=b1/C=c2',
            ]
    
    def path2record(path):
        return {p.split('=')[0]:p.split('=')[1] for p in path.split('/')}
    
    field_list = FieldList('A B C'.split())
    cond = And([
        field_list.A == ('a1 a3'.split()),
        field_list.C == 'c2',
        field_list.B != 'b2',
    ])
    records = {p:path2record(p) for p in paths}
    records
    filtered_path = [p for p, record in records.items() if cond.eval(record, type_conversion=True)]
    filtered_path


Dataframe.query Usage
~~~~~~~~~~~~~~~~~~~~~~~~~
After you create the condition, you can use it to query a dataframe.

.. ipython:: python

    and2.to_df_query() # format as a df query string.
    res = df.query(and2.to_df_query())
    res.tail()

Alternatively:

.. ipython:: python

    res = and2.query(df)
    res.tail()

Pyarrow Partition Filtering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The condition can be converted to and from a pyarrow filter. The filter is passed to 
``pyarrow.parquet.ParquetDataset`` or ``pandas.read_parquet()`` in order
to read only the selected partitions, thereby increase efficiency.

In contrast with the strict structure for pyarrow filters, any condition can be converted to pyarrow filters. 
The condition will be `normalized <usage.html#normalization>`_ first to comply with pyarrow requirements. 

.. note::
    Please note that if a field is not a partition key, its condition will be silently ignored.
    You should follow up with ``condition.query(df)`` to filter out unnecessary rows.

Examples:

.. ipython:: python

    import tempfile
    and1 = And(
            [
                fl.date >= pd.to_datetime('20000101'),
                fl.date <= pd.to_datetime('20000131'),
                fl.A == ('a1 a3'.split()),
                fl.C != ('c3 c5'.split()),
            ]
        )    
    # convert to pyarrow filter
    and1.to_pyarrow_filter()
    # convert back from pyarrow filter
    cond = Condition.from_pyarrow_filter(and1.to_pyarrow_filter())
    print(cond)
    with tempfile.TemporaryDirectory() as t:
        df = df.reset_index()
        df.to_parquet(t, partition_cols=['A', 'C'])
        # fl.date conditions are ignored because 'date' is not a partition key.
        res = pd.read_parquet(t, filters=and1.to_pyarrow_filter()) 
        assert set(res.A.unique()) == set(['a1', 'a3'])
        assert set(res.C.unique()) ^ set(['c3', 'c5'])
        # now the fl.date condition will take effect
        res2 = and1.query(res)
        assert res2.date.min() == pd.to_datetime('20000101')
        assert res2.date.max() == pd.to_datetime('20000131')

Usage Context Extension
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The above usage contexts, even ``visualize()``, are actually implemented as plug-ins to the condition package. 
A plug-in is an implementation of ``ConditionApplication`` which defines behaviors
such as ``on_start``, ``applyFieldCondition``, ``applyAndCondition``, ``applyOrCondition``
and ``on_end``.
You can create your own plug-in by following existing examples. Once you are done, you
can optionally ``register_application`` to use it as if it were built in 
the ``Condition`` class. See test_condition.py for examples.

SQL Generation
-------------------------

Basic SQL
~~~~~~~~~~~~~~~~~~~~~~~~~
The condition can be used to generate sql. ``condition.sql`` package contains a method to
render `jinja2 <https://palletsprojects.com/p/jinja/>`_  sql template. 
You need to install `jinja2 <https://palletsprojects.com/p/jinja/>`_ package before you use it.

.. ipython:: python

    from condition.sql import render_sql
    sql = """
        select *
        from my_table
        where {{where_condition}}
    """
    print(render_sql(sql, and1))


In this example, ``where_condition`` is replaced with a sql clause constructed 
from this condition.

SQL with Column Mappings
~~~~~~~~~~~~~~~~~~~~~~~~~
The fields and the table columns may not be the same.
Also in sql, you may need to use table alias.
In those cases, you can specify a ``dbmap`` parameter
as a dict from a field name to a db column name.

.. ipython:: python

    and2 = and1 & (Field("id") == 'id1')
    print(and2)
    sql = """
        select *
        from my_table t1, my_table2 t2
        where
        t1.id = t2.id
        and {{where_condition}}
    """
    print(render_sql(sql, and2, {'A' : 't1.col1', 'C': 't2.col2', 'id': 't1.id'}))



SQL with Split Conditions
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The ``split()`` method can be used in sql when the sql joins multiple sub queries.


.. ipython:: python

    fl = FieldList('a b c d e'.split())
    cond = And([
        fl.a == ['a1','a2'],
        fl.b > 30,
        fl.d != ['d1', 'd2']
    ])
    sql = """
        select t1.a, b, c, d, e
        from 
            (select a, b, c 
             from my_table 
             where {{ condition.split(['a','b','c']) }}
             ) as t1
        join
            (select a, d, e 
             from my_table2 
             where {{ condition.split(['a','d','e']) }}
             ) as t2
            on t1.a==t2.a
        where {{condition.to_sql_where_condition(db_map=dict(a='t1.a'))}}
        """
    print(render_sql(sql, cond))
    # handle empty condition
    cond = And([
        fl.d != ['d1', 'd2']
    ])
    print(render_sql(sql, cond))


SQL with Custom Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~
For sql, additional parameters can be set and used
in the jinja2 sql template to achieve additional control.


.. ipython:: python

    and1.set_param('use_join_clause', True)

    sql = """
        select *
        from my_table as t1
        {% if use_join_clause -%}
        join my_table2 t2 on t1.fpe=t2.date
        {%- endif %}
        where {{where_condition}}
        """
    print(render_sql(sql, and1))


Now let's turn ``use_join_clause`` off.

.. ipython:: python

    and1.set_param('use_join_clause', False)
    print(render_sql(sql, and1))


As you can see, this clause ``join my_table2 t2 on t1.fpe=t2.date`` is gone.


SQL with Like Condition
~~~~~~~~~~~~~~~~~~~~~~~~~
You may have noticed that a common sql condition, ``like``,
is not supported. It is because this project is geared toward
``dataframe.query()`` which does not support ``like``.
However, it is possible to use the custom parameters to
work around the limitation for sql as shown below:

.. ipython:: python

    and1 = And()
    and1.set_param('col_A_like', 'Par%s')

    sql = """
        select *
        from my_table as t1
        where col_A like '{{col_A_like}}'
        """
    print(render_sql(sql, and1))


This is the end. Hopefully you can find the ``condition`` package
is useful to you.
