# CHANGELOG
### 1.0.8
* Enhance Condition.register_application().
* Remove OrderedSet.
* Sort sub conditions in CompositeCondition and elements for IN/NOT_IN to ensure deterministic order.
### 1.0.7
* Bug fix for repr() for invalid identifiers.
* Refactor to add an extension mechansim through ConditionApplication.
### 1.0.6
* Remove "SQL with Individual Clauses" in favor of split(). 
* Fix split() to use an empty condition instead of None so sql clause can be rendered correctly.
* Allow field to be an invalid identifier, such as a str containing space, '.' and etc.
* Add a test case for pickling.
  
### 1.0.5
* Fix a bug because old version of pyarrow filters requires a set.
* Make '==' and hash() order independent.
* Add typing hints.
  
### 1.0.4
* Add Condition.from_pyarrow_filter()
* Use versioneer to track the versions
  
### 1.0.3
* Define __eq__ and __hash__ for most classes.
* Make condition objects immutable. No need for cycle detection given that.
### 1.0.2
* Make repr() to generate a string which later can be parsed back to a condition object.
* Documentation improvements.
### 1.0.1
* Bug fix: use OrderedDict to ensure the iteration order in condition.to_sql_dict().
* Add Condition.split() to support reducing the conditions for sub data sources.
* Enhance visualize() so it can shown in Jupyter notebook. Also make color/shape changes.
### 1.0.0
Initial release
