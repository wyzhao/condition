# All contribution is welcome.

# On Gitlab Issues and Merge Requests

Found a bug? Have a new feature to suggest? Want to contribute changes to the codebase? Make sure to read this first.

## Installation

Install all packages for development:

```
pip install "condition[dev]"
```

## Bug reporting

Your code doesn't work, and you have determined that the issue lies with Condition package? Follow these steps to report a bug.

1. Your bug may already be fixed. Make sure to update to the current Condition develop branch, as well as the latest master branch.

2. Search for similar issues. Make sure to delete `is:open` on the issue search to find solved tickets as well. It's possible somebody has encountered this bug already. Still having a problem? Open an issue on Gitlab to let us know.

3. Make sure you provide us with useful information about your configuration.

4. provide us with a script to reproduce the issue. This script should be runnable as-is and should not require external data download (use randomly generated data if you need to run a model on some test data). 

5. If possible, take a stab at fixing the bug yourself --if you can!

The more information you provide, the easier it is for us to validate that there is a bug and the faster we'll be able to take action. If you want your issue to be resolved quickly, following the steps above is crucial.


## Requesting a Feature

You can also use Gitlab issues to request features you would like to see in Condition, or changes in the Condition API.

1. Provide a clear and detailed explanation of the feature you want and why it's important to add. Keep in mind that we want features that will be useful to the majority of our users and not just a small subset. If you're just targeting a minority of users, consider writing an add-on library for Condition. It is crucial for Condition to avoid bloating the API and codebase.

2. Provide code snippets demonstrating the API you have in mind and illustrating the use cases of your feature. Of course, you don't need to write any real code at this point!

3. After discussing the feature you may choose to attempt a Merge Request. If you're at all able, start writing some code. We always have more work to do than time to do it. If you can write some code then that will speed the process along.

## Merge Requests

We love merge requests. Here's a quick guide:

1. If your MR introduces a change in functionality, make sure you start by opening an issue to discuss whether the change should be made, and how to handle it. This will save you from having your MR closed down the road! Of course, if your MR is a simple bug fix, you don't need to do that.

2. Write the code. This is the hard part!

3. Make sure any new function or class you introduce has proper docstrings. Make sure any code you touch still has up-to-date docstrings and documentation.

4. Write tests. Your code should have full unit test coverage. If you want to see your MR merged promptly, this is crucial.

5. Run our test suite locally. It's easy: from the Condition folder, simply run: `pytest test/`.
  - You will need to install the test requirements as well: `pip install -e ".[dev]"`.

6. Make sure all tests are passing:

7. We use PEP8 syntax conventions, but we aren't dogmatic when it comes to line length. Make sure your lines stay reasonably sized, though. To make your life easier, we recommend running a PEP8 linter:
  - Install PEP8 packages: `pip install pep8 pytest-pep8 autopep8`
  - Run a standalone PEP8 check: `py.test --pep8 -m pep8`
  - You can automatically fix some PEP8 error by running: `autopep8 -i --select <errors> <FILENAME>` for example: `autopep8 -i --select E128 tests/Condition/backend/test_backends.py`

8. When committing, use apropriate, descriptive commit messages. Make sure that your branch history is not a string of "bug fix", "fix", "oops", etc. When submitting your MR, squash your commits into a single commit with an apropriate commit message, to make sure the MRoject history stays clean and readable. See ['rebase and squash'](http://rebaseandsqua.sh/) for technical help on how to squash your commits.

9. Update the documentation. If introducing new functionality, make sure you include code snippets demonstrating the usage of your new feature.

10. Submit your MR. If your changes have been approved in a previous discussion, and if you have complete (and passing) unit tests, your MR is likely to be merged promptly. Otherwise, well...

## Adding new examples

Even if you don't contribute to the Condition source code, if you have an application of Condition that is concise and powerful, please consider adding it to examples. 
